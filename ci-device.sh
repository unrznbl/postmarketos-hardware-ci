set -e
set -x
set -o pipefail
DEVICE=${1:-hammerhead}
# TODO: add some safety around clobbering/backing up smartly the config
cp $DEVICE-pmbootstrap.cfg ~/.config/pmbootstrap.cfg

echo "Get the sudo token for various operations"
sudo whoami

PMB="../pmbootstrap/pmbootstrap.py"

$PMB pull
$PMB -y zap -hc -d -p -m -o -r

# for true CI will have to figure out how to discriminate between multiple devices plugged in to one computer!
echo "make sure to plug in $DEVICE and get to fastboot mode"
read

# Relies on https://gitlab.com/unrznbl/pmbootstrap/-/tree/install-automation
$PMB install --password y
# for some reason sudo fastboot Oh crap. Why can't each device have the proper steps! :(
# on other/librem I can't do the fastboot bit
$PMB flasher flash_rootfs --partition userdata
#sudo fastboot format system
# only on x220 can I do this step. So plug it in over there when I mess up above. :(
$PMB flasher flash_kernel
fastboot continue
PHONE=user@172.16.42.1
ssh-keygen -f ~/.ssh/known_hosts -R 172.16.42.1
SSH="ssh -o ConnectTimeout=1 -o StrictHostKeyChecking=no $PHONE"
echo "waiting for sshd ready at 172.16.42.1 ... "
while ! $SSH date > /dev/null; do
  sleep 1
done

# start of device interaction, turn off -e to do as much as possible
set +e
yes | $SSH sudo -S sed -i '85s/#//' /etc/sudoers
echo $?
$SSH sudo route add default gw 172.16.42.2
echo $?
$SSH echo nameserver 80.80.80.80 | sudo tee /etc/resolv.conf
echo $?
# setup host side ip forwarding to provide device(s) with usb internet
echo 1 | sudo tee /proc/sys/net/ipv4/ip_forward > /dev/null
sudo iptables -P FORWARD ACCEPT
sudo iptables -A POSTROUTING -t nat -j MASQUERADE -s 172.16.42.0/24

# check device paths for things (are they stable?)
yes | $SSH evtest
echo $?
$SSH ofonoctl poweron
echo $?
#Powered on gobi_0
$SSH ofonoctl online
#Brought gobi_0 online
$SSH ofonoctl list
#Modem    Status        SIM
#-------  ------------  ------
#gobi_0   Unregistered  No SIM
$SSH ofonoctl operators
$SSH nmcli d w connect "$WIFI_SSID" password "$WIFI_PASS"
$SSH ip a
#$SSH sudo reboot-mode bootloader # to ready for next CI run
# but on hammerhead this does not work yet
