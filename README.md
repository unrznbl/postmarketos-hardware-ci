# postmarketos-hardware-ci

CI scripts for testing postmarketOS on hardware devices.

Create a "driver script" something like this:

```
WIFI_SSID=<your wifi ssid>
WIFI_PASS=<your wifi password, or a call to pass or something>
bash ci-device.sh hammerhead
bash ci-device.sh surnia
bash ci-device.sh klte
```

Doing more than one at a time or several in a row in an automated fashion is not supported yet.
